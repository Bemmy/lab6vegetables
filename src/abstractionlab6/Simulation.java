/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractionlab6;

import java.util.*;

public class Simulation {
    
    public static void main(String[] args){
        
        Vegetable beet1 = new Beet("Beet", 1, "Pink");        
        Vegetable beet2 = new Beet("Beet", 2, "Red");
        
        Vegetable carrot1 = new Carrot("Carrot", 1, "Yellow");
        Vegetable carrot2 = new Carrot("Carrot", 1.5, "Orange");
        
        List<Vegetable> veggies = new ArrayList<>();
        veggies.add(beet1);
        veggies.add(beet2);
        veggies.add(carrot1);
        veggies.add(carrot2);
        
        for(Vegetable veg: veggies){
            System.out.println("Name: " + veg.getName() + " | Colour: " + 
                    veg.getColour() + " | Size: " + veg.getSize() + " | " +  
                    veg.isRipe());
        }
    }
    
}
