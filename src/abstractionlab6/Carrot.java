/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractionlab6;

public class Carrot extends Vegetable {
    
    public Carrot(String name, double size, String colour){
        super(name, size, colour);
    }
    
    public String isRipe(){
        
      if(getSize() == 1.5 && getColour().equalsIgnoreCase("Orange")){
           
            return "Carrot is ripe.";
       }
       
       return "Carrot is not ripe.";
   }
}
